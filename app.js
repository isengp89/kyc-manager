const express = require('express');
var path = require('path');
const bodyParser = require('body-parser');
var fileUpload = require("express-fileupload");

const Regisdata = require('./src/model/Regisdata');

const app = express();

const nav = [
    {
        link:'/main',name:'Home'
    },
    {
        link:'/contact',name:'Contact Us'
    }
];
const viewRouter = require('./src/routes/viewRoutes')(nav)

const addRouter = require('./src/routes/addRoutes')(nav)

const updateRouter = require('./src/routes/updateRoutes')(nav)

const deleteRouter = require('./src/routes/deleteRoutes')(nav)

const regRouter = require('./src/routes/regRoutes')(nav)

const mainRouter = require('./src/routes/mainRoutes')(nav)

const adminRouter = require('./src/routes/adminRoutes')(nav)

app.use(bodyParser.json());

app.use(fileUpload());

app.use(express.urlencoded({extended:true}));
app.use(express.static('./public'));

// view engine setup
app.set('view engine','ejs');
app.set('views','./src/views');
app.use('/view',viewRouter);
app.use('/add',addRouter);
app.use('/update',updateRouter);
app.use('/delete',deleteRouter);
app.use('/signup',regRouter);
app.use('/main',mainRouter);
app.use('/admin',adminRouter);

app.get('/',function(req,res){
    res.render("index",
    {
        title: 'KYC Manager'
    });
});

// User Login using MongoDB
app.post('/login',function(req,res){
    var email = req.body.email;
    var password = req.body.password;


    Regisdata.findOne({email:email,password:password},(err,match)=>{
        if(err) 
        {
            console.log("Error")
        }
        if(!match)
        {
            res.render("index",
            {
                title: 'KYC Manager', 
            });
        }
        else {
            if(email=='admin' && password=='123456')
            res.render("admin",
            {
                title: 'KYC Manager', 
            });
            else
            res.redirect("/main");
        }
    })
});

app.get('/contact',function(req,res){
    res.render("contact",
    {
        title: 'KYC Manager'
    });
});

//-------------------WEB3 Integration using Infura starts-----------------------

Web3 = require("web3");

var MyContractJSON = require(path.join(__dirname, 'build/contracts/KYC.json'));

const HDWalletProvider = require('@truffle/hdwallet-provider');
 
const infuraKey = "d4e3db976b2c48f2a59205c5eb9b4172";
const address_index= 0;
const num_addresses= 5;
const mnemonic = 'fit monitor sing dash old mom lava vendor left bullet once ride';


const provider = new HDWalletProvider(mnemonic, `https://ropsten.infura.io/v3/${infuraKey}`, address_index, num_addresses);
web3 = new Web3(provider);

contractAddress = MyContractJSON.networks['3'].address;
var contractAbi = MyContractJSON.abi;

MyContract = new web3.eth.Contract(contractAbi, contractAddress);
console.log(MyContract.methods);

//-------------------WEB3 Integration Stops------------------------

app.listen(5000);