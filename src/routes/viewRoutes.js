var express = require('express');
var viewRouter = express.Router();
var ipfsClient = require('ipfs-http-client');
Web3 = require("web3");

function router(nav){
    viewRouter.get('/',function(req,res){
        res.render("view",{
            nav,
            title: 'KYC Manager'
        })
    })

    // GET KYC in string format
    viewRouter.get('/detail', function(req, res, next) {
        var uid = req.query.uid;
        let bdata = Web3.utils.utf8ToHex(uid);
        let byt = web3.eth.abi.encodeParameter('bytes32', bdata);
        web3.eth.getAccounts().then((accounts) =>{
            MyContract.methods.getKycDetails(byt)
            .call({from:accounts[0],gas:3000000})
            .then((result) => {
                console.log(result);
                var str1 = Web3.utils.toUtf8(result[0]);
                var str2 = Web3.utils.toUtf8(result[1]);
                var str3 = Web3.utils.toUtf8(result[2]);
                var str4 = Web3.utils.toUtf8(result[3]);
                var str5 = Web3.utils.toUtf8(result[4]);
                var str6 = Web3.utils.toUtf8(result[5]);
                var str7 = Web3.utils.toUtf8(result[6]);
                var str8 = Web3.utils.toUtf8(result[7]);
                if(result[8]==false){
                    var str9 = "Not Verified"
                }
                else{
                var str9 = "Verified"
                }

                res.render("viewdet", {
                    nav,
                    result : [str1,str2,str3,str4,str5,str6,str7,str8,str9],
                    uid : uid,
                    title:'KYC Manager'
                });
            });
        });
    });

    // GET PAN doc from IPFS
    viewRouter.get('/viewDoc', function (req, res, next) {
        var uid1 = req.query.uid1;  
        web3.eth.getAccounts().then((accounts) =>{
            MyContract.methods.documentHash(uid1)
            .call({ from: accounts[0] }).then((val) => {
                console.log(val);
                ipfspath = 'https://ipfs.io/ipfs/' + web3.utils.hexToUtf8(val);
                res.redirect(ipfspath);
            });
        });
    });
    return viewRouter;
}
  
module.exports = router;