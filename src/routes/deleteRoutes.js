var express = require('express');
var deleteRouter = express.Router();
Web3 = require("web3");
const signTxn = require("./signTxn");

function router(nav){
    deleteRouter.post('/', function(req, res, next) {
        var uid = req.body.uid1;
        let bdata = Web3.utils.utf8ToHex(uid);
        let byt = web3.eth.abi.encodeParameter('bytes32', bdata);
        var methodCall = MyContract.methods.deleteKycDetails(byt);
        signTxn.sendTransaction(methodCall,function(response){
            if(response==true)res.send("KYC Deleted Successfully");
            else res.send("Transaction failed")
        });
    });
    return deleteRouter;
}
  
module.exports = router;