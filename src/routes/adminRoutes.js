var express = require('express');
var adminRouter = express.Router();
Web3 = require("web3");
const signTxn = require("./signTxn");

function router(nav){

    adminRouter.get('/',function(req,res){
        res.render("admin",{
            nav,
            title: 'KYC Manager'
        })
    });

    adminRouter.get('/adminview', function(req, res, next) {
        var uid = req.query.uid;
        let bdata = Web3.utils.utf8ToHex(uid);
        let byt = web3.eth.abi.encodeParameter('bytes32', bdata);
        web3.eth.getAccounts().then((accounts) =>{
            MyContract.methods.getKycDetails(byt)
            .call({from:accounts[0],gas:3000000})
            .then((result) => {
                console.log(result);
                var str1 = Web3.utils.toUtf8(result[0]);
                var str2 = Web3.utils.toUtf8(result[1]);
                var str3 = Web3.utils.toUtf8(result[2]);
                var str4 = Web3.utils.toUtf8(result[3]);
                var str5 = Web3.utils.toUtf8(result[4]);
                var str6 = Web3.utils.toUtf8(result[5]);
                var str7 = Web3.utils.toUtf8(result[6]);
                var str8 = Web3.utils.toUtf8(result[7]);
                if(result[8]==false){
                    var str9 = "Not Verified"
                }
                else{
                var str9 = "Verified"
                }

                res.render("adminview", {
                    nav,
                    result : [str1,str2,str3,str4,str5,str6,str7,str8,str9],
                    uid : uid,
                    title:'KYC Manager'
                });
            });
        });
    });

    adminRouter.post('/adminview/verify', function(req, res, next) {
        data=req.body;
        console.log(data);
        let bdata1 = Web3.utils.utf8ToHex(data.uid);
        let byt1 = web3.eth.abi.encodeParameter('bytes32', bdata1);
  
        var methodCall = MyContract.methods.verify(byt1);
        signTxn.sendTransaction(methodCall,function(response){
            if(response==true)res.send("KYC Verified");
            else res.send("Transaction failed")
        });
    });

    // GET Event details from Blockchain
    adminRouter.get('/getEvents', function(req, res, next) {
        data= req.query.uid1;
        MyContract.getPastEvents('AllEvents',{
        fromBlock:0   ,
        toBlock:'latest'
        },(err,events)=>{
            console.log("====>events",events)
            eventArr=[];
            for (var i = 0; i <= (events.length - 1); i++) {

                eventArr.push({ id: i, transactionhash: events[i].transactionHash, blocknumber: events[i].blockNumber, uid:  Web3.utils.toUtf8(events[i].returnValues[0]), eventdet: events[i].returnValues[1]});
                if (eventArr.length == events.length)
                    res.render("events", { eventArray: eventArr })
            }
        });
    });
    return adminRouter;
}

module.exports = router;
