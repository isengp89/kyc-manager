var express = require('express');
var addRouter = express.Router();
var ipfsClient = require('ipfs-http-client');
Web3 = require("web3");

const signTxn = require("./signTxn");
function router(nav){
    addRouter.get('/',function(req,res){
        res.render("add",{
            nav,
            title: 'KYC Manager'
        })
    })

    // POST KYC in bytes32 format
    addRouter.post('/new', function(req, res, next) {
        data=req.body;
        console.log(data);
        let bdata1 = Web3.utils.utf8ToHex(data.uid);
        let byt1 = web3.eth.abi.encodeParameter('bytes32', bdata1);
    
        let bdata2 = Web3.utils.utf8ToHex(data.firstname);
        let byt2 = web3.eth.abi.encodeParameter('bytes32', bdata2);
        
        let bdata3 = Web3.utils.utf8ToHex(data.gender);
        let byt3 = web3.eth.abi.encodeParameter('bytes32', bdata3);
        
        let bdata4 = Web3.utils.utf8ToHex(data.dob);
        let byt4 = web3.eth.abi.encodeParameter('bytes32', bdata4);
        
        let bdata5 = Web3.utils.utf8ToHex(data.nation);
        let byt5 = web3.eth.abi.encodeParameter('bytes32', bdata5);

        let bdata6 = Web3.utils.utf8ToHex(data.income);
        let byt6 = web3.eth.abi.encodeParameter('bytes32', bdata6);
        
        let bdata7 = Web3.utils.utf8ToHex(data.email);
        let byt7 = web3.eth.abi.encodeParameter('bytes32', bdata7);
        
        let bdata8 = Web3.utils.utf8ToHex(data.tel);
        let byt8 = web3.eth.abi.encodeParameter('bytes32', bdata8);
        
        let bdata9 = Web3.utils.utf8ToHex(data.addr);
        let byt9 = web3.eth.abi.encodeParameter('bytes32', bdata9);

        let byt10 = data.verifystatus;
  
        var methodCall = MyContract.methods.addKycDetails(byt1,byt2,byt3,byt4,byt5,byt6,byt7,byt8,byt9,byt10);
        signTxn.sendTransaction(methodCall,function(response){
            if(response==true)res.send("KYC Added Successfully");
            else res.send("Transaction failed")
        });
    });

    // POST PAN document to IPFS
    addRouter.post('/upload', async function (req, res, next) {
        uid1 = req.body.uid1;
        fileBytes = req.files.document.data;

        console.log(fileBytes);
      
        const ipfs = ipfsClient('/ip4/127.0.0.1/tcp/5001');
      
        const result = await ipfs.add(fileBytes)
        console.log(result.path);
        
        var methodCall = MyContract.methods.setDocumentHash(uid1, web3.utils.toHex(result.path));
        signTxn.sendTransaction(methodCall,function(response){
            if(response==true)res.send("Document uploaded with IPFS Hash: " + result.path);
            else res.send("Transaction failed")
        });
    });
    return addRouter;
}
  
module.exports = router;