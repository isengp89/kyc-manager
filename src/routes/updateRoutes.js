var express = require('express');
var updateRouter = express.Router();
Web3 = require("web3");
const signTxn = require("./signTxn");

function router(nav){
    updateRouter.get('/',function(req,res){
        var uid = req.query.uid;
        var firstname = req.query.firstname;
        var gender = req.query.gender;
        var dob = req.query.dob;
        var nation = req.query.nation;
        var income = req.query.income;
        var email = req.query.email;
        var tel = req.query.tel;
        var addr = req.query.addr;
        res.render("update",{
            nav,
            title: 'KYC Manager',
            uid:uid,firstname:firstname,gender:gender,dob:dob,nation:nation,income:income,email:email,tel:tel,addr:addr
        })
    })

    // POST KYC in bytes32 format
    updateRouter.post('/amend', function(req, res, next) {
        data=req.body;
        console.log(data);
        let bdata1 = Web3.utils.utf8ToHex(data.uid);
        let byt1 = web3.eth.abi.encodeParameter('bytes32', bdata1);
    
        let bdata2 = Web3.utils.utf8ToHex(data.firstname);
        let byt2 = web3.eth.abi.encodeParameter('bytes32', bdata2);
        
        let bdata3 = Web3.utils.utf8ToHex(data.gender);
        let byt3 = web3.eth.abi.encodeParameter('bytes32', bdata3);
        
        let bdata4 = Web3.utils.utf8ToHex(data.dob);
        let byt4 = web3.eth.abi.encodeParameter('bytes32', bdata4);
        
        let bdata5 = Web3.utils.utf8ToHex(data.nation);
        let byt5 = web3.eth.abi.encodeParameter('bytes32', bdata5);

        let bdata6 = Web3.utils.utf8ToHex(data.income);
        let byt6 = web3.eth.abi.encodeParameter('bytes32', bdata6);
        
        let bdata7 = Web3.utils.utf8ToHex(data.email);
        let byt7 = web3.eth.abi.encodeParameter('bytes32', bdata7);
        
        let bdata8 = Web3.utils.utf8ToHex(data.tel);
        let byt8 = web3.eth.abi.encodeParameter('bytes32', bdata8);
        
        let bdata9 = Web3.utils.utf8ToHex(data.addr);
        let byt9 = web3.eth.abi.encodeParameter('bytes32', bdata9);
  
        var methodCall = MyContract.methods.updateKycDetails(byt1,byt2,byt3,byt4,byt5,byt6,byt7,byt8,byt9);
        signTxn.sendTransaction(methodCall,function(response){
            if(response==true)res.send("KYC Updated Successfully");
            else res.send("Transaction failed")
        });
    });
    return updateRouter;
}
  
module.exports = router;