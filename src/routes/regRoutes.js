const express = require('express');
const Regisdata = require('../model/Regisdata');

const regRouter = express.Router();

function router(nav){
    regRouter.get('/',function(req,res){
        res.render("signup",{
            nav,
            title: 'KYC Manager'
        })
    });

    // POST Users credentials to MONGO DB
    regRouter.post('/reg',function(req,res){
        var item = {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            city: req.body.city,
            state: req.body.state,
        }
        var register = Regisdata(item);
        register.save();
        res.redirect('/');
    })

    return regRouter;
}

module.exports = router;


