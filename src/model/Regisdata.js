const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/kycDB');

const Schema = mongoose.Schema;

const RegisSchema = new Schema({
    name: String,
    email: String,
    password: String,
    city: String,
    state: String,
});

var Regisdata = mongoose.model('register',RegisSchema);

module.exports = Regisdata;