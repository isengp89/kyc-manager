// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.7.0;

contract KYC {
    
    struct details{
        bytes32 uid;
        bytes32 firstname;
        bytes32 gender;
        bytes32 dob;
        bytes32 nation;
        bytes32 income;
        bytes32 email;
        bytes32 tel;
        bytes32 addr;
        bool verifystatus;
    }
    

    details[] public customers;

    uint256 public totalCustomers;
    
    address public owner;
    
    constructor() {
        totalCustomers = 0;
        owner = msg.sender;    
    }
    
    modifier isOwner() {
        require(msg.sender == owner, "Not owner");
        _;
    }
  
    string public addmsg = "Added KYC";
    string public updmsg = "Updated KYC";
    string public delmsg = "Deleted KYC";
    string public vermsg = "Verified";
    
    event AddEvent (bytes32 indexed uid, string addmsg);
    
    event UpdateEvent(bytes32 indexed uid, string updmsg);
    
    event DeleteEvent(bytes32 indexed uid, string delmsg);
    
    event Verified(bytes32 indexed uid, string vermsg);
    
    mapping(bytes32 => details) profile;
    mapping (bytes32 => bool) isTaken;

    function isSet(bytes32 _uid) public view returns(bool isIndeed) {
        return (isTaken[_uid]);
    }
    
    function addKycDetails(bytes32 _uid, bytes32 _firstname, bytes32 _gender, bytes32 _dob, bytes32 _nation, bytes32 _income, bytes32 _email, bytes32 _tel, bytes32 _addr, bool _verifystatus) public returns(bool success) {
        require(!isSet(_uid));
        profile[_uid] = details(_uid, _firstname, _gender, _dob, _nation, _income, _email, _tel, _addr, _verifystatus);
        details memory p = profile[_uid];
        customers.push(p);
        totalCustomers++;
        emit AddEvent (_uid, addmsg);
        isTaken[_uid] = true;
        return true;
    }
    
    function updateKycDetails(bytes32 _uid, bytes32 newfirstname, bytes32 newgender, bytes32 newdob, bytes32 newnation, bytes32 newincome, bytes32 newemail, bytes32 newtel, bytes32 newaddr) isOwner public returns (bool success){
       for(uint256 i =0; i< totalCustomers; i++){
           if(compareCust(customers[i].uid ,_uid)){
              customers[i].firstname = newfirstname;
              customers[i].gender = newgender;
              customers[i].dob = newdob;
              customers[i].nation = newnation;
              customers[i].income = newincome;
              customers[i].email = newemail;
              customers[i].tel = newtel;
              customers[i].addr = newaddr;
              customers[i].verifystatus = false;
              emit UpdateEvent(_uid, updmsg);
              return true;
           }
       }
       return false;
   }
   
   function deleteKycDetails(bytes32 _uid) isOwner public returns(bool success){
        require(totalCustomers > 0);
        for(uint256 i =0; i< totalCustomers; i++){
           if(compareCust(customers[i].uid , _uid)){
              customers[i] = customers[totalCustomers-1]; // pushing last into current arrray index which we gonna delete
              delete customers[totalCustomers-1]; // now deleteing last index
              totalCustomers--; //total count decrease
              //emit event
              emit DeleteEvent(_uid, delmsg);
              isTaken[_uid] = false;
              return true;
           }
       }
       return false;
   }

    function getKycDetails(bytes32 _uid) public view returns(bytes32, bytes32, bytes32, bytes32, bytes32, bytes32, bytes32, bytes32, bool) {
        for(uint256 i =0; i< totalCustomers; i++){
           if(compareCust(customers[i].uid ,_uid)){
              return (customers[i].firstname,customers[i].gender,customers[i].dob,customers[i].nation,customers[i].income,customers[i].email,customers[i].tel,customers[i].addr,customers[i].verifystatus);
           }
       }
       revert('customer not found');
        
    }
    
    function verify(bytes32 _uid) public returns (bool success){
       //This has a problem we need loop
       for(uint256 i =0; i< totalCustomers; i++){
           if(compareCust(customers[i].uid ,_uid)){
              customers[i].verifystatus = true;
              emit Verified(_uid, vermsg);
              return true;
           }
       }
       return false;
   }
    
    function compareCust(bytes32 a, bytes32 b)  internal pure returns (bool){
       return a == b;
   }
    
    mapping(string => bytes) public documentHash;
    mapping (string => bool) isTakendoc;
    
    function isSetdoc(string memory _uid1) public view returns(bool isIndeed) {
        return (isTakendoc[_uid1]);
    }
    
    function setDocumentHash(string memory _uid1,bytes memory _documentHash) public returns(bool success) {
        require(!isSetdoc(_uid1));
        documentHash[_uid1] = _documentHash;
        isTakendoc[_uid1] = true;
        return true;
    }
}
