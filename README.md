# KYC Manager Project

## Setting Up:

**Step 1:** Download the repository using the command:  
```
 Git Clone "https://gitlab.com/isengp89/kyc-manager.git"  
 ```
**Step 2:** Setup MongoDB Compass to store details of registered users 

**Step 3:** Install the dependancies using the command: 
```
 npm Install  
 ```
**Step 4:** Use the following command to compile smart contract:  
```
 truffle compile  
 ```
**Step 5:** Use one of the following commands to connect to the private or public chain:   

 
**Step 6:** Use the following command to deploy the smart contract: 
```
 truffle migrate --network ropsten  
 ```
**Step 7:** Run the dapp using the command  
```
 node app.js  
```
## Execution Flow:

**Step 8:** Go http://localhost:5000/register, Register as a financial Institution/Bank (User details stored in MongoDB Compass)
```
ex:  
  Name: ICICI    
  CIN: L65190GJ1994PLC021012  
  Password: Hello@123  
  Corporate Ofice: Mumbai  
  Telephone: 0471-28912  
``` 
**Step 9:** Go http://localhost:5000, Login with registered credentials
```
ex:  
  Username (CIN): L65190GJ1994PLC021012      
  Password: Hello@123  
``` 
**Step 10:** Go http://localhost:5000/add, Provide the  details and click Add Details. Also upload document by inputing Id no. of ID proof.
```
ex:
Add KYC Details  
  Name of Customer: James  
  PAN No.: HH2345  
  DOB: 01-05-2005  
  Gender: Male  
  Nationality: Indian  
  Income: <5 Lakh
  Email: james@yahoo.com  
  Telephone: 04628462  
  Address: Pattom, Trivandrum

Upload Document
  Proof of Identity No.: PAN/Aadhaar/Passport
``` 
**Step 11:** Go to http://localhost:3000/view, enter PAN No to view the KYC Details and Documents that have been added previously and click view. 
```

END