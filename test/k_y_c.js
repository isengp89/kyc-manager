const KYC = artifacts.require("KYC");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("KYC", function () {
  it("should assert true", async function () {
    await KYC.deployed();
    return assert.isTrue(true);
  });

  it("Add student detail checking", async function () {
    kyc = await KYC.deployed();

    uid = "0x6131323300000000000000000000000000000000000000000000000000000000";
    firstname = "0x6a6f686e00000000000000000000000000000000000000000000000000000000";
    gender = "0x6d616c6500000000000000000000000000000000000000000000000000000000";
    dob = "0x30352f30352f3230303500000000000000000000000000000000000000000000";
    nation = "0x496e6469616e0000000000000000000000000000000000000000000000000000";
    income = "0x31206c616b680000000000000000000000000000000000000000000000000000";
    email = "0x6973656e40676d61696c00000000000000000000000000000000000000000000";
    tel = "0x3034373932303731000000000000000000000000000000000000000000000000";
    addr = "0x54726976616e6472756d00000000000000000000000000000000000000000000";
    
    await kyc.addKycDetails(uid,firstname,gender,dob,nation,income,email,tel,addr);
    registeredDetails = await kyc.getKycDetails(uid);
    assert.equal(registeredDetails[0], firstname, "Test Fail");
    assert.equal(registeredDetails[1], gender, "Test Fail");
    assert.equal(registeredDetails[2], dob, "Test Fail");
    assert.equal(registeredDetails[3], nation, "Test Fail");
    assert.equal(registeredDetails[4], income, "Test Fail");
    assert.equal(registeredDetails[5], email, "Test Fail");
    assert.equal(registeredDetails[6], tel, "Test Fail");
    assert.equal(registeredDetails[7], addr, "Test Fail");

    return assert.isTrue(true);
  });

  it("duplication checking", async function () {
    kyc = await KYC.deployed();
    
    uid="0x6131323300000000000000000000000000000000000000000000000000000000";
    isTaken = await kyc.isSet(uid);
    
    assert.isTrue(isTaken, 'the details for the UID have not been stored');

  });

  it("duplication checking", async function () {
    kyc = await KYC.deployed();

    uid="0x6131323400000000000000000000000000000000000000000000000000000000";
    isTaken = await kyc.isSet(uid);
    
    assert.isFalse(isTaken, 'the details for the UID have already been stored');

    return assert.isTrue(true);
  });


});
